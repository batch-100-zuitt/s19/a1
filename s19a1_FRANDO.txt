6.Get users IDs and add them as enrollees of the courses (update):
db.courses.updateOne({_id: courses1}, {$set: {enrollees:[user1,user3,user5]}});
db.courses.updateOne({_id: courses2}, {$set: {enrollees:[user2,user4,user1]}});
db.courses.updateOne({_id: courses3}, {$set: {enrollees:[user5,user6]}});





7. Get the names of the users who are not administrators
db.user.find({"isAdmin": false},{"FirstName":1});  
    
8. Aggregate through the users collection using field projection and sorting
db.user.aggregate([
	{
	$project:
		{
		FirstName: "$FirstName",
		_id : 0
		}
		},
		{ $sort: { FirstName: 1}}
	
	]);
9. Use the count operator to count the total number of users on isActive
db.user.count({isActive:true})


10. Show students who are not active in their course, without showing the course IDs. 
db.user.find({"isActive": false},{"enrollments":0});  


